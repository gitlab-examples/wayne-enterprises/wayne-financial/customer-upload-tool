module gitlab.com/gitlab-org/defend/demos/container-host-security-demo

go 1.14

require (
	github.com/gin-contrib/sse v0.1.1-0.20190905051334-43f0f29dbd2b // indirect
	github.com/gin-gonic/gin v1.6.3
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/json-iterator/go v1.1.10 // indirect
	github.com/mattn/go-isatty v0.0.13-0.20200128103942-cb30d6282491 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	golang.org/x/sys v0.0.0-20200610111108-226ff32320da // indirect
	gopkg.in/yaml.v2 v2.3.0 // indirect
)
