package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"path"

	"github.com/gin-gonic/gin"
)

var db = make(map[string]string)
var applicationPassword string

func prepareEnvironment() {
	os.MkdirAll("/tmp/uploads", 644)
}

func setupRouter() *gin.Engine {
	r := gin.Default()

	authorized := r.Group("/", gin.BasicAuth(gin.Accounts{
		"demo": applicationPassword,
	}))

	r.LoadHTMLGlob("templates/*")

	r.GET("/probe", func(c *gin.Context) {
		c.HTML(http.StatusOK, "probe.tmpl", gin.H{})
	})

	authorized.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.tmpl", gin.H{})
	})

	authorized.GET("/ping", func(c *gin.Context) {
		c.HTML(http.StatusOK, "ping.tmpl", gin.H{})
	})

	authorized.POST("/ping", func(c *gin.Context) {
		ip := c.PostForm("ip")

		cmd := exec.Command("bash", "-c", fmt.Sprintf("ping -c 4 %s", ip))
		cmdOutput := &bytes.Buffer{}
		cmd.Stdout = cmdOutput

		var result string

		if err := cmd.Run(); err != nil {
			result = string(err.Error())
		} else {
			result = string(cmdOutput.Bytes())
		}

		c.HTML(http.StatusOK, "ping.tmpl", gin.H{
			"result": result,
		})
	})

	authorized.GET("/upload", func(c *gin.Context) {
		c.HTML(http.StatusOK, "upload.tmpl", gin.H{})
	})

	authorized.POST("/upload", func(c *gin.Context) {
		filename := c.PostForm("filename")

		if file, err := c.FormFile("file"); err != nil {
			c.String(http.StatusNotFound, "File not properly uploaded")
		} else {
			fmt.Println(path.Join("/", "tmp", "uploads", filename))
			err = c.SaveUploadedFile(file, path.Join("/", "tmp", "uploads", filename))

			if err != nil {
				c.String(http.StatusUnprocessableEntity, "File was not uploaded!")
			} else {
				c.HTML(http.StatusOK, "upload.tmpl", gin.H{
					"file":     file.Filename,
					"filepath": filename,
				})
			}
		}
	})

	authorized.GET("/uploads", func(c *gin.Context) {
		filename, _ := c.GetQuery("filename")
		if len(filename) > 0 {
			filepath := fmt.Sprintf("/tmp/uploads/%s", filename)

			if _, err := ioutil.ReadFile(filepath); err != nil {
				c.String(http.StatusNotFound, "File not found")
			} else {
				c.File(filepath)
			}
		} else {
			files, _ := ioutil.ReadDir("/tmp/uploads/")

			filenames := make([]string, len(files))
			for index, file := range files {
				filenames[index] = file.Name()
			}

			c.HTML(http.StatusOK, "uploads.tmpl", gin.H{
				"files": filenames,
			})
		}
	})

	return r
}

func main() {
	prepareEnvironment()
	r := setupRouter()

	r.Run(":5000")
}
