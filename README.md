# GitLab Container Host Security Demo

This project contains the application that on purpose has some security issues and allows you to check how Container Host Security can detect malicious behavior in the application.

It can be used to test [falco](https://falco.org/) integration deployed using [cluster management](https://docs.gitlab.com/ee/user/clusters/applications.html#install-falco-using-gitlab-cicd) approach.

## Name and Namespace

Applications will be deployed to Kubernetes with service names and namespaces not known prior deployment. After querying the desired services (e.g., `kubectl get svc --all-namespaces`), this information can be used as an input to this project.

## Demo URL

You can find the available deployment addresses for this application in the [environments](https://gitlab.com/gitlab-org/protect/demos/container-host-security/-/environments) page of this project.

Because this is application is vulnerable to security issues (by design), it is password-protected in public environments. The user name is hard-coded to `demo`.

The password, if defined, can be found in the variables section of the [CI/CD settings](https://gitlab.com/gitlab-org/protect/demos/container-host-security/-/settings/ci_cd).
